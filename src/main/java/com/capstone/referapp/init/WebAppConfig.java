package com.capstone.referapp.init;

import java.util.Locale;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.capstone.referapp.common.AppConfigConstants;
import com.capstone.referapp.common.AppConfigReader;

@Configuration
@ComponentScan("com.capstone.referapp")
@EnableWebMvc
@EnableTransactionManagement
public class WebAppConfig extends WebMvcConfigurerAdapter {

	final Logger LOG = LoggerFactory.getLogger(WebAppConfig.class);

	@Autowired
	AppConfigReader configReader;
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/i18/messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver resolver = new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("en"));
		resolver.setCookieName("myLocaleCookie");
		resolver.setCookieMaxAge(4800);
		return resolver;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		interceptor.setParamName("mylocale");
		registry.addInterceptor(interceptor);
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName(configReader.getConfig(AppConfigConstants.CONFIG_DB_DRIVER));
		dataSource.setUrl(configReader.getConfig(AppConfigConstants.CONFIG_DB_URL));
		dataSource.setUsername(configReader.getConfig(AppConfigConstants.CONFIG_DB_USERNAME));
		dataSource.setPassword(configReader.getConfig(AppConfigConstants.CONFIG_DB_PASSWORD));

		return dataSource;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean lsFactoryBean = new LocalSessionFactoryBean();
		lsFactoryBean.setDataSource(dataSource());
		lsFactoryBean.setPackagesToScan(configReader.getConfig(AppConfigConstants.CONFIG_ORM_MODEL_PKG));
		lsFactoryBean.setHibernateProperties(hibProperties());
		return lsFactoryBean;
	}

	private Properties hibProperties() {
		Properties properties = new Properties();
		properties.put(AppConfigConstants.CONFIG_ORM_DIALECT, configReader.getConfig(AppConfigConstants.CONFIG_ORM_DIALECT));
		properties.put(AppConfigConstants.CONFIG_ORM_SHOW_SQL, configReader.getConfig(AppConfigConstants.CONFIG_ORM_SHOW_SQL));
		return properties;
	}

	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager transMgr = new HibernateTransactionManager();
		transMgr.setSessionFactory(sessionFactory().getObject());
		return transMgr;
	}

	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		LOG.info("initializing view resolver...");
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/pages/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		LOG.info("initialized view resolver");
		return resolver;
	}


	/**
	 * Configure ResourceHandlers to serve static resources like CSS/ Javascript
	 * etc...
	 */
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
		
	}
}