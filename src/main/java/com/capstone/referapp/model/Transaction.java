package com.capstone.referapp.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transaction")
public class Transaction {
	
	@Id
	@GeneratedValue
	@Column(name="trans_Id")
    private int trans_Id;
	
	@Column(name="credit_No")
    private int creditNo;
	
	@Column(name="trans_Date")
   private Date trans_Date;
	
	@Column(name="trans_Amount")
private float trans_Amount;
	
	@Column(name="total_cycle")
private int total_cycle;
	
	@Column(name="emi")
private float emi;
	
	@Column(name="status")
private int Status;
	
	public int getTotal_cycle() {
		return total_cycle;
	}
	public void setTotal_cycle(int total_cycle) {
		this.total_cycle = total_cycle;
	}
	public int getStatus() {
		return Status;
	}
	public void setStatus(int status) {
		Status = status;
	}

	public int getTrans_Id() {
		return trans_Id;
	}
	public void setTrans_Id(int trans_Id) {
		this.trans_Id = trans_Id;
	}
	public int getCreditNo() {
		return creditNo;
	}
	public void setCreditNo(int creditNo) {
		this.creditNo = creditNo;
	}
	public Date getTrans_Date() {
		return trans_Date;
	}
	public void setTrans_Date(Date trans_Date) {
		this.trans_Date = trans_Date;
	}
	public float getTrans_Amount() {
		return trans_Amount;
	}
	public void setTrans_Amount(float trans_Amount) {
		this.trans_Amount = trans_Amount;
	}
		public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
		

}
