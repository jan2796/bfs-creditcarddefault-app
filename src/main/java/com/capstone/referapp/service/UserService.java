package com.capstone.referapp.service;

import java.sql.SQLException;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;

import com.capstone.referapp.dao.UserBO;
import com.capstone.referapp.dao.UserDAO;
import com.capstone.referapp.model.Admin;
import com.capstone.referapp.model.Creditcard;
import com.capstone.referapp.model.User;


@Service
@Transactional
public class UserService {

	@Autowired
	UserBO userBO;
	
	public boolean checkuser(User user){
		return userBO.checkuser(user);
	}
	 public int getCid(int id)
	 {
		 return userBO.getCid(id);
	 }

		
		public boolean checkadmin(Admin ad) {
			return userBO.checkadmin(ad);		}
		public String getmail(int user_Id) {
			return userBO.getMail(user_Id);
		}
		public String getpass(int user_Id) {
			return userBO.getPass(user_Id);

		}
		public void errorcheck(User us, BindingResult errors) {
        
		ValidationUtils.rejectIfEmpty(errors,"user_Id","600","CANNOT BE EMPTY");
		ValidationUtils.rejectIfEmpty(errors,"password","601","CANNOT BE EMPTY");

		}
	
	}
