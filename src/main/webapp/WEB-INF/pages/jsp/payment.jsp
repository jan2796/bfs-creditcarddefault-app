<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>


	<link href="<c:url value='/static/css/animate.css' />"
	rel="stylesheet"></link>
	
	<!-- Custom Stylesheet -->
	<link href="<c:url value='/static/css/style2.css' />"
	rel="stylesheet"></link>

</head>
<body>
<h1 align="center">PAYMENT DETAILS</h1>
<table align="center" width=80%>

<tr style="background-color:teal;">
<td width="15%">Payment Id</td>
				<td width="15%">Transaction Id</td>
				<td width="25%">Amount Paid</td>
				<td width="10%">Payment Date</td>
				<td width="10%">Cycle No</td>
				
</tr>
<c:forEach var="transaction" items="${trans}">
<c:if test="${transaction.days>30}">
<tr style="background-color:red">
</c:if>
				<td>${transaction.pay_Id}</td>
				<td>${transaction.trans_Id}</td>
				<td><fmt:formatNumber type="number" maxFractionDigits="0" value="${tr.amount_Paid}"/></td>
				<td><fmt:formatDate type="date" 
            value="${transaction.payment_Date}"/></td>
				<td>${transaction.cycle_No}</td></tr>
		
				
				</c:forEach>
				</table>
				<div align="center"><a href="${pageContext.request.contextPath}/home.html">Log Out</a>
				
				</div>
				<div align="center"><a href="${pageContext.request.contextPath}/jsp/alltransactions.html">Back</a>
				
				</div>
				
				</body>
				</html>