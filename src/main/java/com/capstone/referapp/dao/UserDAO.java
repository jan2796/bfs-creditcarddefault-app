package com.capstone.referapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.referapp.model.Admin;
import com.capstone.referapp.model.Creditcard;
import com.capstone.referapp.model.Payment;
import com.capstone.referapp.model.Transaction;
import com.capstone.referapp.model.User;
import com.mysql.jdbc.PreparedStatement;

@Repository
public class UserDAO implements UserBO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	public boolean checkuser(User user) {
	
	  Session s=getSession();
		boolean flag=false;
		String SQL_QUERY =" from User as o where o.user_Id=? and o.password=?";
		Query query = s.createQuery(SQL_QUERY);
		query.setParameter(0,user.getUser_Id());
		query.setParameter(1,user.getPassword());

		List<User> list = query.list();

		if ((list != null) && (list.size() > 0)) {
			flag=true;
		}
		
return flag;
	}
	public int getCid(int id) {
		int flag;
		String SQL_QUERY ="from User as o where o.user_Id=?";
		Query query = getSession().createQuery(SQL_QUERY);
		query.setParameter(0,id);
		List<User> list=query.list();
		User u=list.get(0);
		flag=u.getCredit_No();

	
		return flag;
		
	}
	
	public Creditcard getcredit(int cid){
		Creditcard card = (Creditcard) getSession().get(Creditcard.class,cid);
		float total=gettotal(cid);
		float totalcredit=(float) card.getCreditLimit();
		
		if(total==totalcredit)
			{
			card.setStatus(1);
			}
		else
			card.setStatus(0);
return card;  
	}
	
	@Override
	public List<Transaction> gettrans(int cid) {
		String SQL_QUERY ="from Transaction as t where t.creditNo=?";
		Query query = getSession().createQuery(SQL_QUERY);
		query.setParameter(0,cid);
		List<Transaction> list=query.list();
		int cycles;
		for(Transaction t:list)
		{
			cycles=getpaycycles(t.getTrans_Id());
			if(t.getTotal_cycle()!=0)
			{
				if(t.getTotal_cycle()==cycles)
			       t.setStatus(1);
				else
					t.setStatus(0);
			}
			else
				if(t.getTotal_cycle()==0 && cycles==1)
					t.setStatus(1);
				else
					t.setStatus(0);							
		}
		
		return list;

	}
	
	
	public int getpaycycles(Integer trans_Id) {
		String SQL_QUERY ="from Payment as p where p.trans_Id=?";
		Query query = getSession().createQuery(SQL_QUERY);
		query.setParameter(0,trans_Id);
		List<Payment> list=query.list();
		int size=list.size();
		return size;

	}
	
	@Override
	public float gettotal(int cid) {
		String SQL_QUERY ="from Transaction as t where t.creditNo=?";
		Query query = getSession().createQuery(SQL_QUERY);
		query.setParameter(0,cid);
		List<Transaction> list=query.list();
		float totalamount=0;
		for(Transaction t:list)
		{
			totalamount=totalamount+t.getTrans_Amount();
		}
		return totalamount;

	}

	
	@Override
	public Transaction getsingletr(Integer trans_Id) {
		Transaction transaction = (Transaction) getSession().get(Transaction.class,trans_Id);
		return transaction;  

	}
	
	
	
	@Override
	public void updatetr(Transaction trans) {
		Transaction transaction = getsingletr(trans.getTrans_Id());
		transaction.setTotal_cycle(trans.getTotal_cycle());
		float interest=(transaction.getTrans_Amount()*5)/100;
		float amount=interest+transaction.getTrans_Amount();
	    float emi=(amount/transaction.getTotal_cycle());
	    transaction.setEmi(emi);
		getSession().update(transaction);

	    

		
	}
	@Override
	public boolean checkadmin(Admin ad) {
		boolean flag=false;
		if(ad.getAdmin_Id()==0000 && ad.getPassword().equals("admin"))
		{
			flag=true;
		}
		
return flag;

	}
	
	@Override
	public List<Transaction> getalltr() {
		List<Transaction> list=getSession().createQuery("from Transaction").list();
		int cycles;
		for(Transaction t:list)
		{
			cycles=getpaycycles(t.getTrans_Id());
			if(t.getTotal_cycle()!=0)
			{
				if(t.getTotal_cycle()==cycles)
			       t.setStatus(1);
				else
					t.setStatus(0);
			}
			else
				if(t.getTotal_cycle()==0 && cycles==1)
					t.setStatus(1);
				else
					t.setStatus(0);
							
		}
		return list;

			}
	
	@Override
	public List<Payment> getpay(Integer trans_Id) {
		String SQL_QUERY ="from Payment as p where p.trans_Id=?";
		Query query = getSession().createQuery(SQL_QUERY);
		query.setParameter(0,trans_Id);
		List<Payment> list=query.list();
		
		Date dateArray[]=new Date[10];
        int iterator=0;
		for(Payment n:list)
		{  Date date;
		  date=n.getPayment_Date();
		  dateArray[iterator]=date;
		  iterator++;
			}
		int NoOfDays=iterator;
		int jloop=0;
		int kloop=1;
		int days[]=new int[10];
		for(jloop=1;jloop<NoOfDays;jloop++)
		{
				        int diffInDays = (int) ((dateArray[jloop].getTime() - dateArray[jloop-1].getTime()) / (1000 * 60 * 60 * 24));
		days[kloop]=diffInDays;
		kloop++;
		}
		days[0]=0;
		iterator=0;
		for(Payment nm:list)
		{ nm.setDays(days[iterator]);
		iterator++;
		}

		
		
		return list;	}
	
	@Override
	public int getcycles(Integer trans_Id) {
		Transaction transaction = (Transaction) getSession().get(Transaction.class,trans_Id);
		int cycles=transaction.getTotal_cycle();
		return cycles;  
	}
	@Override
	public String getMail(int user_Id) {
		User user = (User) getSession().get(User.class,user_Id);
        String mailid=user.getEmail();
		return mailid;
	}
	@Override
	public String getPass(int user_Id) {

		User user = (User) getSession().get(User.class,user_Id);
        String password=user.getPassword();
		return password;
	}

	
	}

				
