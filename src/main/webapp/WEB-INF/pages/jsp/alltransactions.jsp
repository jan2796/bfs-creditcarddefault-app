<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>


	<link href="<c:url value='/static/css/animate.css' />"
	rel="stylesheet"></link>
	
	<!-- Custom Stylesheet -->
	<link href="<c:url value='/static/css/style2.css' />"
	rel="stylesheet"></link>
<title>Insert title here</title>
</head>
<body>
<h1 align="center">TRANSACTION DETAILS</h1>
<table align="center" width=80%>
<tr style="background-color:teal;">
<td width="15%">Transaction_Id</td>
				<td width="15%">Creditcard No</td>
				<td width="25%">Transaction Date</td>
				<td width="10%">Transaction Amount</td>
				<td width="10%">Emi Cycle</td>
				<td width="10%">Emi Amount</td>
				<td width="10%" align="center">Transaction Status</td>
</tr>
<c:forEach var="tr" items="${trans}">
<tr>
				<td>${tr.trans_Id}</td>
				<td>${tr.creditNo}</td>
				<td><fmt:formatDate type="date" 
            value="${tr.trans_Date}"/></td>
				<td><fmt:formatNumber type="number" maxFractionDigits="0" value="${tr.trans_Amount}"/></td>
				<td>${tr.total_cycle}</td>
				<td>${tr.emi}</td>
				<c:if test="${tr.status==1}">
				<td>CLOSED</td>
					</c:if>
			<c:if test="${tr.status==0}">
			<td>OPEN</td>
			    			      </c:if>
				<td><a href="${pageContext.request.contextPath}/jsp/payment/${tr.trans_Id}.html">Payment</a><br />
													</td>
				
				</tr>
				</c:forEach>
				</table>
				<div align="center" style="font-size:150%;color:white ;font-family:times new roman;"><a href="${pageContext.request.contextPath}/home.html"><u>Log Out</u></a>
				
				</div>
				</body>
				</html>