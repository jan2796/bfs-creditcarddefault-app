package com.capstone.referapp.service;


	import java.util.Properties;

	import javax.mail.Message;
	import javax.mail.MessagingException;
	import javax.mail.PasswordAuthentication;
	import javax.mail.Session;
	import javax.mail.Transport;
	import javax.mail.internet.InternetAddress;
	import javax.mail.internet.MimeMessage;

	import org.springframework.stereotype.Service;
	import org.springframework.transaction.annotation.Transactional;

	@Service
	@Transactional
	public class EmailService
	{
		@Transactional
		public static void sendmail(String mailid,String pass,int id) {
     
			final String username = "creditcarddefaultsystem@gmail.com";
			final String password = "defaultsystem";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress("from-email@gmail.com"));
				message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(mailid));
				message.setSubject("Credit Card password recollect");
				message.setText("Dear Customer,"
					+ "\n\n   Your Userid and password !!"
					+ "\nUserId: "+id
					+ "\nPassword: "+pass);

				Transport.send(message);

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		}
	}

