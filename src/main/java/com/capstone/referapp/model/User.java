package com.capstone.referapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue
	@Column(name="user_Id")
	private int user_Id;
	
	@Column(name="password")
	private String password;
	
	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "User [user_Id=" + user_Id + ", password=" + password + ", email=" + email + ", credit_No=" + credit_No
				+ "]";
	}

	public void setEmail(String email) {
		this.email = email;
	}



	@Column(name="email_Id")
	   private String email;
	
	@Column(name="credit_No")
	private int credit_No;

	

	
	
	
	public int getUser_Id() {
		return user_Id;
	}

	public void setUser_Id(int user_Id) {
		this.user_Id = user_Id;
	}

	public int getCredit_No() {
		return credit_No;
	}

	public void setCredit_No(int credit_No) {
		this.credit_No = credit_No;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
		
	public User() {
		// TODO Auto-generated constructor stub
	}

}
