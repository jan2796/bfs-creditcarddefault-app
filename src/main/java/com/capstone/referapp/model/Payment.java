package com.capstone.referapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="payment")
public class Payment {
	
	@Id
	@GeneratedValue
	@Column(name="pay_Id")
    private int pay_Id;

	@Column(name="trans_Id")
    private int trans_Id;
	
	@Column(name="amount_Paid")
    private float amount_Paid;
	
	@Column(name="payment_Date")
    private Date payment_Date;
	
	@Column(name="cycle_No")
	private int cycle_No;
	
	@Column(name="days")
	private int days;

	
	
	
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public int getPay_Id() {
		return pay_Id;
	}
	public void setPay_Id(int pay_Id) {
		this.pay_Id = pay_Id;
	}
	public int getTrans_Id() {
		return trans_Id;
	}
	public void setTrans_Id(int trans_Id) {
		this.trans_Id = trans_Id;
	}
		public Date getPayment_Date() {
		return payment_Date;
	}
	public void setPayment_Date(Date payment_Date) {
		this.payment_Date = payment_Date;
	}
	public float getAmount_Paid() {
		return amount_Paid;
	}
	public void setAmount_Paid(float amount_Paid) {
		this.amount_Paid = amount_Paid;
	}
	public int getCycle_No() {
		return cycle_No;
	}
	public void setCycle_No(int cycle_No) {
		this.cycle_No = cycle_No;
	}

}
