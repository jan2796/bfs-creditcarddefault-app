package com.capstone.referapp.dao;

import java.sql.SQLException;
import java.util.List;

import com.capstone.referapp.model.Admin;
import com.capstone.referapp.model.Creditcard;
import com.capstone.referapp.model.Payment;
import com.capstone.referapp.model.Transaction;
import com.capstone.referapp.model.User;



public interface UserBO {

	
	boolean checkuser(User user);

	int getCid(int id);
 Creditcard getcredit(int cid);
 List<Transaction> gettrans(int cid);
 Transaction getsingletr(Integer trans_Id);

void updatetr(Transaction trans);

boolean checkadmin(Admin ad);

List<Transaction> getalltr();

List<Payment> getpay(Integer trans_Id);

int getcycles(Integer trans_Id);

float gettotal(int cid);

String getMail(int user_Id);

String getPass(int user_Id);



}
