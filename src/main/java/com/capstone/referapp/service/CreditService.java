package com.capstone.referapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.referapp.dao.UserBO;
import com.capstone.referapp.model.Creditcard;

@Service
@Transactional
public class CreditService {
	
	@Autowired
	UserBO userBO;

	public Creditcard getcredit(int cid){
		return userBO.getcredit(cid);
	}

}
