DROP TABLE IF EXISTS creditcard;
DROP TABLE IF EXISTS transaction;
DROP TABLE IF EXISTS payment;
DROP TABLE IF EXISTS user;

CREATE TABLE creditcard(
   credit_No INT(8) primary key,
   owner VARCHAR(50) not null,
   credit_Limit float(12,2),
   status int(2) not null default 1
   
);

CREATE TABLE transaction(
    trans_Id INT(10) primary key auto_increment,
    credit_No INT(8) not null references creditcard(credit_No),
    trans_Date DATE not null,
    trans_Amount FLOAT(12,2) not null,
    total_cycle INT(3) not null,
    emi FLOAT(12,2) not null,
    status int(2) not null default 1
   
   );


CREATE TABLE payment(
      pay_Id INT(12) auto_increment primary key ,
      trans_Id INT(10)  REFERENCES transaction(trans_Id),
      amount_Paid FLOAT(12,2) not null,
      payment_Date DATE not null,
      cycle_No INT(3) 

  );


CREATE TABLE user(
      user_Id int(8) not null,
      password VARCHAR(20) not null,
      type int(2) not null,
      credit_No int(8) references creditcard(credit_No),
      primary key(user_id,password)
    
);