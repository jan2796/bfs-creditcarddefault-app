<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>


	<link href="<c:url value='/static/css/animate.css' />"
	rel="stylesheet"></link>
	
	<!-- Custom Stylesheet -->
	<link href="<c:url value='/static/css/style4.css' />"
	rel="stylesheet"></link>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js">

</script>
</head>

<body>
	<div class="container">
		<div class="top">
		<p></p>
		<p></p>
			<h1 id="title" class="hidden"><span id="logo">RECOVERY<span></span></span></h1>
		</div>
		<div class="login-box animated fadeInUp">
		
		   <form:form action="email" method="POST" commandName="foremail" >
			
			<label for="username">Enter User Id :</label>
			<br/>
			<form:input type="text" id="username" path="user_Id"/>
		
			<br/>
			<button type="submit">Recover Password</button>
			<br/>
			</form:form>
		</div>
	</div>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>



</html>