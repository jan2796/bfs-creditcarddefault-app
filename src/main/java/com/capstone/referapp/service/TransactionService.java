package com.capstone.referapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.referapp.dao.UserBO;
import com.capstone.referapp.model.Transaction;

@Service
@Transactional
public class TransactionService {
	
	@Autowired
	private UserBO ub;
	
	public List<Transaction> gettr(int cid){
		return ub.gettrans(cid);
	}

	public Transaction getsingletr(Integer trans_Id) {
		return ub.getsingletr(trans_Id);
	}




	public void update(Transaction trans) {
		 ub.updatetr(trans);
		
	}

	public List<Transaction> getalltr() {
		 	return ub.getalltr();
	}

	public int getcycles(Integer trans_Id) {
		return ub.getcycles(trans_Id);	}
	
	
	
}
