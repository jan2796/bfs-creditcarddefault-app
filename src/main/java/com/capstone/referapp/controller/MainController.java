package com.capstone.referapp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capstone.referapp.model.Admin;
import com.capstone.referapp.model.Creditcard;
import com.capstone.referapp.model.Payment;
import com.capstone.referapp.model.Transaction;
import com.capstone.referapp.model.User;
import com.capstone.referapp.service.CreditService;
import com.capstone.referapp.service.EmailService;
import com.capstone.referapp.service.PaymentService;
import com.capstone.referapp.service.TransactionService;
import com.capstone.referapp.service.UserService;

@Controller
@RequestMapping(value = "/jsp")
public class MainController {
	
	@Autowired
	private UserService userserv;
	
	@Autowired
	private EmailService emailserv;

	
	@Autowired
	private TransactionService transserv;
	
	@Autowired
	private PaymentService payserv;
	
	@Autowired
	private CreditService creditserv;

    @RequestMapping(value = "email",method=RequestMethod.POST)
	public String sendMail(@ModelAttribute User us, ModelMap model) 
	{
		String emailid=userserv.getmail(us.getUser_Id());
		String pass=userserv.getpass(us.getUser_Id());
		int id=us.getUser_Id();
		emailserv.sendmail(emailid,pass,id);
		model.addAttribute("message","Mail has been sent to your Email-Id  : "+emailid);
		return "jsp/login";	
	}
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public final String email(ModelMap model){
		model.addAttribute("foremail", new User());
		 return "jsp/view";
     }
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public final String login(ModelMap model) {
		model.addAttribute("user", new User());
		model.addAttribute("message"," ");
		return "jsp/login";
	}
	
	@RequestMapping(value = "check", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute User us, ModelMap model,BindingResult errors) {
		
		
		String message = "Invalid credentials";
		model.addAttribute("message",message);
		String page="jsp/login";
        userserv.errorcheck(us, errors);
		if(userserv.checkuser(us) )
		{
			page="jsp/details";
			int cid=userserv.getCid(us.getUser_Id());
			Creditcard c=creditserv.getcredit(cid);
           			model.addAttribute("card",c);

           
return page;
		}
		
return page;		
			}
	@RequestMapping(value = "/alogin", method = RequestMethod.GET)
	public final String alogin(ModelMap model) {
		model.addAttribute("admin", new Admin());
		return "jsp/alogin";
	}
	
	@RequestMapping(value = "acheck",method = RequestMethod.POST)
	public String saveadmin(@ModelAttribute Admin ad, ModelMap model) {
		String message = "Invalid credentials";
		String page="jsp/alogin";
		if(userserv.checkadmin(ad) )
		{
List<Transaction> trans=transserv.getalltr();
model.addAttribute("trans",trans);
			page="jsp/alltransactions";
		}       
return page;
		}

	@RequestMapping(value = "/payment/{trans_Id}", method = RequestMethod.GET)
	public final String getpay(@PathVariable final Integer trans_Id, ModelMap model) throws ParseException {
List<Payment> pay=payserv.getpay(trans_Id);

model.addAttribute("trans",pay);
		return "jsp/payment";
	}
	
	@RequestMapping(value = "/alltransactions", method = RequestMethod.GET)
	public final String addp(ModelMap model) {
		List<Transaction> trans=transserv.getalltr();
		model.addAttribute("trans",trans);
		return "jsp/alltransactions";
	}

	
	@RequestMapping(value = "/transactions/{creditNo}", method = RequestMethod.GET)
	public final String addpage(@PathVariable final Integer creditNo, ModelMap model) {
List<Transaction> trans=transserv.gettr(creditNo);
model.addAttribute("trans",trans);
		return "jsp/transactions";
	}

	
	

		
	@RequestMapping(value = "/emi/{trans_Id}", method = RequestMethod.GET)
	public final String editTeamPage(@PathVariable final Integer trans_Id, ModelMap model) {
		Transaction t = transserv.getsingletr(trans_Id);
				model.addAttribute("option", t);
		return "jsp/emi";
	}

	@RequestMapping(value = "/emi/{trans_Id}", method = RequestMethod.POST)
	public final String edditingTeam(@PathVariable final Integer trans_Id, @ModelAttribute final Transaction trans, ModelMap model) {
		
    	transserv.update(trans);
    	Transaction t=transserv.getsingletr(trans_Id);
		String message = "EMI option was added to Transaction -------"+trans_Id+"------";
		model.addAttribute("message", message);
		model.addAttribute("creditNo",t.getCreditNo());
		return "jsp/result";
	}
	
	
	

	

}
