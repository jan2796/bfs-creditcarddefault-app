<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>


	<link href="<c:url value='/static/css/animate.css' />"
	rel="stylesheet"></link>
	
	<!-- Custom Stylesheet -->
	<link href="<c:url value='/static/css/style1.css' />"
	rel="stylesheet"></link>
<title>Insert title here</title>
<style>
.style
{
color:black;
}
button
{
border: 0 none;
background:white;
box-shadow: 0 0 2px 2px white;
border-radius: 5px;
}
</style>
</head>
<body>
<div class="top">
			<h1 id="title" class="hidden">CREDIT CARD DETAILS</h1>
		</div>
			<div class="style">

<table style="background-image:url=('../images/cc.png')" width=50% align="center">
<tr><td><b>Credit Card Number :</b></td> <td><b>${card.creditNo}</b></td></tr>
<tr></tr>
<tr><td><b>Card Holder Name  :</b></td> <td><b>${card.owner}</b></td></tr>
<tr></tr>
<tr><td><b>Credit Limit Amount :</b></td> <td><b>${card.creditLimit}</b></td></tr>

<tr><td><b>Card Status :</b></td> 
<c:if test="${card.status==0}">
<td><b>OPEN</b></td>
</c:if>
<c:if test="${card.status==1}">
<td><b>CLOSED</b></td>
</c:if>
</tr>
<tr>
<td colspan="2" width="200px"  align="center">
<br>
<div>
<form action="${pageContext.request.contextPath}/jsp/transactions/${card.creditNo}.html"> 
<button type="submit"> My Transactions</button>
</form>
</div>
</td>
</tr>
</table>
</div>
<div align="center"><a href="${pageContext.request.contextPath}/home.html">Log Out</a>
				
				</div>

</body>
</html>