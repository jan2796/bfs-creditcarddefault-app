package com.capstone.referapp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="creditcard")
public class Creditcard {
	@Id
	@GeneratedValue
	@Column(name="credit_No")
	private int creditNo;
	
		@Column(name="owner")
    private String owner;
	
	@Column(name="credit_Limit")
     private double creditLimit;
	
	@Column(name="status")
    private int status;
	
	public int getStatus() {
		return status;
	}
	public void setCreditNo(int creditNo) {
		this.creditNo = creditNo;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCreditNo() {
		return creditNo;
	}
		public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public double getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
	
	@Override
	public String toString() {
		return "Creditcard [creditNo=" + creditNo + ", owner=" + owner + ", creditLimit=" + creditLimit + ", Status="
				+ status + "]";
	}

		

}
