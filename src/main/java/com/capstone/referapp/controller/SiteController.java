package com.capstone.referapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capstone.referapp.common.AppConfigConstants;
import com.capstone.referapp.common.AppConfigReader;

@Controller
public class SiteController {
	
	@Autowired
	AppConfigReader configReader;
	
	@RequestMapping(value = "/")
	public String mainPage(ModelMap model) {
		String prop = configReader.getConfig(AppConfigConstants.CUSTOM_DATE_FORMAT);
		model.put("dformat", prop);
		return "home";
	}

	@RequestMapping(value = "/home")
	public final String homepage(ModelMap model) {
		return "home";
	}
	
}

